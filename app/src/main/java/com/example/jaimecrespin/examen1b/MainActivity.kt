package com.example.jaimecrespin.examen1b

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {


    internal lateinit var inicioJuego: Button
    internal lateinit var adivinarNumero: Button
    internal lateinit var puntajeText: TextView
    internal lateinit var randomText: TextView


    internal lateinit var contador: CountDownTimer
    internal val contadorInicial = 10000L
    internal val intervalo = 1000L
    internal var empezoJuego = false
    internal var timeleft = 10
    internal var n =0
    internal var puntaje =0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        inicioJuego=findViewById(R.id.inicioJuego)
        adivinarNumero=findViewById(R.id.adivinarNumero)
        puntajeText= findViewById(R.id.puntajeText)
        randomText=findViewById(R.id.random)

        puntajeText.text=getString(R.string.puntaje,puntaje.toString())
        randomText.text =getString(R.string.random_number,n.toString())

        inicioJuego.setOnClickListener{ _  -> startGame()}
        adivinarNumero.setOnClickListener{ _  -> adivinar()}
    }

    @SuppressLint("StringFormatInvalid")
    private fun adivinar(){
        if(n == timeleft){
            puntaje = 100
        }else if (n <= timeleft+1 && n >= timeleft-1){
            puntaje = 50
        }else{
            puntaje = 0
        }
        puntajeText.text = getString(R.string.puntaje, puntaje.toString())

        if(!empezoJuego){
            startGame()
        }
        contador.cancel()
    }

    private fun iniciarContador(){
        contador = object : CountDownTimer(contadorInicial, intervalo){
            override fun onTick(millisUntilFinished: Long) {
                timeleft = millisUntilFinished.toInt() / 1000
            }
            override fun onFinish() {
            restartGame()
            }
        }
    }

    private fun startGame(){
            puntaje=0
            puntajeText.text=getString(R.string.puntaje,puntaje.toString())
            iniciarContador()
            contador.start()
            n = (0.. 10).shuffled().first()
            empezoJuego = true
            randomText.text =getString(R.string.random_number,n.toString())

        }

    private fun restartGame(){
            timeleft = 10
            n = 0
            empezoJuego = false
        }

}
